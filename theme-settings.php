<?php

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function wwuzen_dual_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  if ($form_state == NULL) {
    $form = array('');
  }

  $form['theme_settings']['department_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Department Name'),
    '#description' => t('The department to which this dual theme is assigned.'),
    '#default_value' => theme_get_setting('department_name'),
  );
}
